#!/usr/bin/python
'''
Displays a progress bar for FFMPEG

Usage:
    Pipe the output of an ffmpeg script into it (both stdout and stderr!)
    ffmpeg -i input.avi output.mp4 |& python ffmpeg-progress.py

Known Issues:
    - Does not handle prompts from ffmpeg
    - No error reporting
'''

import os
import sys
import re
import fcntl
import select
from progressbar import * 

widgets = ['Encoding: ', Percentage(), ' ', Bar(), ' ', ETA()]
duration_re = re.compile(r'Duration: ([0-9:.]+)')
time_re = re.compile('time=\s*([0-9:.]+) ')

def parse_time(time):
    h, m, s = [float(x) for x in time.split(':')]
    return h * 3600 + m * 60 + s

if __name__ == '__main__':
    duration = None
    pbar = ProgressBar(widgets=widgets, maxval=100).start()
    # ffmpeg does not flush
    fcntl.fcntl(sys.stdin.fileno(), fcntl.F_SETFL,
                fcntl.fcntl(sys.stdin.fileno(), fcntl.F_GETFL) | os.O_NONBLOCK)
    while True:
        read = select.select([sys.stdin.fileno()], [], [])[0]
        if read:
            data = sys.stdin.read()
            if not data: break
            if duration is None:
                m = duration_re.search(data)
                if m:
                    duration = parse_time(m.group(1))
            else:
                m = time_re.search(data)
                if m:
                    time = parse_time(m.group(1))
                    # ffmpeg sometimes goes over causing ProgressBar errors
                    pbar.update(min((time / duration) * 100, 100))
    pbar.finish()

